﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsGestionMedicamentDB.Models;

namespace WindowsFormsGestionMedicamentDB.Repository
{
    internal class VenteRepository
    {

        public List<Vente> venteList = new List<Vente>();
        int nbrVente;
        SqlConnection con = DAO.con;
        public void CreateVente(Vente vente)
        {
            string query = "INSERT INTO ventes (medicament_id,client_id,quantite,created_at) VALUES (@Medicament_Id, @Client_Id, @Quantite, @Created_at)";
            SqlCommand command = new SqlCommand(query, con);
            command.Parameters.AddWithValue("@Medicament_Id", vente.Medicament_Id);
            command.Parameters.AddWithValue("@Client_Id", vente.Client_Id);
            command.Parameters.AddWithValue("@Quantite", vente.Quantite);
            command.Parameters.AddWithValue("@Created_at", vente.Created_at);
            con.Open();
            command.ExecuteNonQuery();
            con.Close();
        }
        public List<Vente> AfficherVentes()
        { 
            string query = "SELECT * FROM Ventes";
            SqlCommand command = new SqlCommand(query, con);
            con.Open();
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Vente vente = new Vente();
                vente.Id = (int)reader["Id"];
                vente.Medicament_Id = (int)reader["medicament_id"];
                vente.Client_Id = (int)reader["client_id"];
                vente.Created_at = (DateTime)reader["Created_at"];
                vente.Quantite = (int)reader["Quantite"];
                venteList.Add(vente);
            }
            reader.Close();
            con.Close();
            return venteList;
        }

        public void UpdateVente(Vente vente)
        {
            string query = "UPDATE ventes SET medicament_id = @Medicament_Id, client_id = @Client_Id, Created_at = @Created_at, Quantite = @Quantite WHERE Id = @Vente_Id";
            SqlCommand command = new SqlCommand(query, con);
            command.Parameters.AddWithValue("@Medicament_Id", vente.Medicament_Id);
            command.Parameters.AddWithValue("@Client_Id", vente.Client_Id);
            command.Parameters.AddWithValue("@Created_at", vente.Created_at);
            command.Parameters.AddWithValue("@Quantite", vente.Quantite);
            command.Parameters.AddWithValue("@Vente_Id", vente.Id);
            con.Open();
            command.ExecuteNonQuery();
            con.Close();
        }

        public void DeleteVente(int venteId)
        {
            string query = "DELETE FROM ventes WHERE Id = @Vente_Id";
            SqlCommand command = new SqlCommand(query, con);
            command.Parameters.AddWithValue("@Vente_Id", venteId);
            con.Open();
            command.ExecuteNonQuery();
            con.Close();
        }

        public int nombreVente()
        {
            string query = "SELECT COUNT(*) as 'nbr'  FROM ventes";
            SqlCommand cmd = new SqlCommand(query, con);
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                nbrVente = (int)reader["nbr"];
            }

            reader.Close();
            con.Close();
            return nbrVente;
        }
    }
}
