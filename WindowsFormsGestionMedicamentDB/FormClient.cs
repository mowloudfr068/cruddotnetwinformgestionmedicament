﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsGestionMedicamentDB.Models;
using WindowsFormsGestionMedicamentDB.Repository;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace WindowsFormsGestionMedicamentDB
{
    public partial class FormClient : Form
    {
        bool isModified = false;
        ClientRepository clientRepository;
        public FormClient()
        {
            InitializeComponent();
            clientRepository = new ClientRepository();

        }

        private void FormClient_Load(object sender, EventArgs e)
        {
            textBoxCl1.Enabled = false;
            textBoxCl2.Enabled = false;
            textBoxCl3.Enabled = false;
            loadClient();
        }

        public void loadClient()
        {
            List<Client> clientList = clientRepository.AfficherClient();
            dataGridViewCl1.DataSource = clientList;
        }
        private void buttonCl4_Click(object sender, EventArgs e)
        {
            textBoxCl1.Enabled = true;
            textBoxCl2.Enabled = true;
            textBoxCl3.Enabled = true;
        }

        private void buttonCl1_Click(object sender, EventArgs e)
        {

            //---------
            if (!isModified)
            {
                //
                if (textBoxCl1.Text != "" && textBoxCl2.Text != "" && textBoxCl3.Text != "")
                {
                    int telephone;
                    if (int.TryParse(textBoxCl3.Text, out telephone))
                    {
                        //--
                        Client client = new Client()
                        {
                            Nom = textBoxCl1.Text,
                            Prenom = textBoxCl2.Text,
                            Telephone = Convert.ToInt32(textBoxCl3.Text),
                        };
                        clientRepository.createClient(client);
                        dataGridViewCl1.DataSource = null;
                        loadClient();
                        MessageBox.Show("client ajouter avec succes", "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //--

                    }
                    else
                    {
                        MessageBox.Show("Le telephone doivent etre numerique", "Ereur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Veuillez remplir tous les champs", "Ereur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                if (dataGridViewCl1.SelectedRows.Count > 0)
                {
                    if (textBoxCl1.Text != "" && textBoxCl2.Text != "" && textBoxCl3.Text != "")
                    {
                        int telephone;
                        if (int.TryParse(textBoxCl3.Text, out telephone))
                        {
                            //--
                            int selectedRowIndex = dataGridViewCl1.SelectedRows[0].Index;
                    int clientId = (int)dataGridViewCl1.Rows[selectedRowIndex].Cells["Id"].Value;
                    Client client = new Client()
                    {
                        Id = clientId,
                        Nom = textBoxCl1.Text,
                        Prenom = textBoxCl2.Text,
                        Telephone = Convert.ToInt32(textBoxCl3.Text),
                    };
                    clientRepository.UpdateClient(client);
                    dataGridViewCl1.DataSource = null;
                    loadClient();
                    MessageBox.Show("le client est modifier avec succee", "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //--
                        }
                        else
                        {
                            MessageBox.Show("Le telephone doivent etre numerique", "Ereur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Veuillez remplir tous les champs", "Ereur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
            }


            }

        private void buttonCl3_Click(object sender, EventArgs e)
        {
            if (dataGridViewCl1.Rows.Count > 0)
            {
                isModified = true;
                Client client = clientRepository.clientList[dataGridViewCl1.CurrentCell.RowIndex];
                textBoxCl1.Enabled = true;
                textBoxCl2.Enabled = true;
                textBoxCl3.Enabled = true;
                textBoxCl1.Text = client.Nom;
                textBoxCl2.Text = client.Prenom.ToString();
                textBoxCl3.Text = client.Telephone.ToString();
            }
        }

        private void buttonCl2_Click(object sender, EventArgs e)
        {
            if (dataGridViewCl1.SelectedRows.Count > 0)
            {
                int selectedRowIndex = dataGridViewCl1.SelectedRows[0].Index;
                int medicamentId = (int)dataGridViewCl1.Rows[selectedRowIndex].Cells["Id"].Value;
                clientRepository.deleteClient(medicamentId);
                dataGridViewCl1.DataSource = null;
                loadClient();
                MessageBox.Show("la suppression de medicament est fait avec succee", "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information);


            }
        }

        private void homeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormApp formApp = new FormApp();
            formApp.Show();
            this.Close();
        }
    }
}
