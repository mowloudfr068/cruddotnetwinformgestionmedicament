﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsGestionMedicamentDB.Models;

namespace WindowsFormsGestionMedicamentDB.Repository
{
    internal class ClientRepository
    {
        SqlConnection con = DAO.con;
        public List<Client> clientList = new List<Client> ();
        public int nbrClient;
        public List<Client> AfficherClient()
        {
            string query = "SELECT * FROM clients";
            SqlCommand cmd = new SqlCommand (query, con);
            con.Open ();
            cmd.ExecuteNonQuery();
            SqlDataReader reader = cmd.ExecuteReader ();
            while (reader.Read())
            {
                Client client = new Client ();
                client.Id = (int)reader["Id"];
                client.Nom = reader["Nom"].ToString();
                client.Prenom = reader["Prenom"].ToString();
                client.Telephone = (int)reader["Telephone"];
                clientList.Add (client);
            }
            reader.Close();
            con.Close();
            return clientList;
            
        }
        public void createClient(Client client)
        {
            string query = "INSERT INTO clients(Nom,Prenom,Telephone) VALUES(@Nom,@Prenom,@Telephone)";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@Nom", client.Nom);
            cmd.Parameters.AddWithValue("@Prenom", client.Prenom);
            cmd.Parameters.AddWithValue("@Telephone", client.Telephone);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public void UpdateClient(Client client)
        {
            string query = "UPDATE clients SET Nom = @Nom, Prenom = @Prenom, Telephone = @Telephone WHERE Id =@Id";
            SqlCommand command = new SqlCommand(query, con);
            command.Parameters.AddWithValue("@Nom", client.Nom);
            command.Parameters.AddWithValue("@Prenom", client.Prenom);
            command.Parameters.AddWithValue("@Telephone", client.Telephone);
            command.Parameters.AddWithValue("@Id", client.Id);
            con.Open();
            command.ExecuteNonQuery();
            con.Close();
        }

        public void deleteClient(int id)
        {
            string query = "DELETE FROM clients WHERE Id=@Id";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@Id", id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public int nombreClient()
        {
            string query = "SELECT COUNT(*) as 'nbr'  FROM clients";
            SqlCommand cmd = new SqlCommand(query, con);
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                nbrClient = (int)reader["nbr"];
            }

            reader.Close();
            con.Close();
            return nbrClient;
        }

    }
}
