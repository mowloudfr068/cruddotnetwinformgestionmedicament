﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsGestionMedicamentDB.Models;
using WindowsFormsGestionMedicamentDB.Repository;

namespace WindowsFormsGestionMedicamentDB
{
    public partial class FormApp : Form
    {
        MedicamentRepository medicamentRepository;
        ClientRepository clientRepository;
        VenteRepository venteRepository = new VenteRepository();
        public FormApp()
        {
            InitializeComponent();
            medicamentRepository = new MedicamentRepository();
            clientRepository = new ClientRepository();
            venteRepository = new VenteRepository();
        }

        private void medicamentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormApp formApp = new FormApp();

            FormMedicament formMedicament = new FormMedicament();
            formMedicament.Show();
            this.Close();
        }

        private void clientsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormApp formApp = new FormApp();
            FormClient formClient = new FormClient();
            formClient.Show();
            this.Close();
        }

        private void ventesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormVente formVente = new FormVente();
            formVente.Show();
            this.Close();

        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
           
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            
        }

        private void FormApp_Load(object sender, EventArgs e)
        {
            pictureBox1.Image = Image.FromFile("C:\\Users\\Mohamed Mowloud\\source\\repos\\cruddotnetwinformgestionmedicament\\WindowsFormsGestionMedicamentDB\\Resources\\399712.png");
            int nbrMedicament = medicamentRepository.nombreMedicament();
            label4.Text = nbrMedicament.ToString();
            int nbrClient = clientRepository.nombreClient();
            label5.Text = nbrClient.ToString();
            int nbrVente = venteRepository.nombreVente();
            label6.Text = nbrVente.ToString();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
