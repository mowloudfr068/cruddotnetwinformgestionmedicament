﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using WindowsFormsGestionMedicamentDB.Models;

namespace WindowsFormsGestionMedicamentDB.Repository
{
    internal class MedicamentRepository
    {
        SqlConnection con = DAO.con;

        public List<Medicament> medicamentList = new List<Medicament>();
        int nbrMedicament;
        public void createMedicament(Medicament medicament)
        {
            string query = "INSERT INTO medicaments(Libelle,Prix,Quantite) VALUES(@Libelle,@Prix,@Quantite)";

            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@Libelle", medicament.Libelle);
            cmd.Parameters.AddWithValue("@Prix",medicament.Prix);
            cmd.Parameters.AddWithValue("@Quantite", medicament.Quantite);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public List<Medicament> AfficherMedicament()
        {

            string query = "SELECT * FROM medicaments";
            SqlCommand cmd = new SqlCommand(query,con);
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Medicament medicament = new Medicament();
                medicament.Id = (int)reader["Id"];
                medicament.Libelle = (string)reader["Libelle"];
                medicament.Prix = (int)reader["Prix"];
                medicament.Quantite = (int)reader["Quantite"];
                medicamentList.Add(medicament);
            }
            reader.Close();
            con.Close();
            return medicamentList;

        }

        public void UpdateMedicament(Medicament medicament)
        {
            string query = "UPDATE medicaments SET Libelle= @Libelle,Prix=@Prix,Quantite=@Quantite WHERE Id=@Id";
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@Libelle",medicament.Libelle);
            cmd.Parameters.AddWithValue("@Prix",medicament.Prix);
            cmd.Parameters.AddWithValue("@Quantite", medicament.Quantite);
            cmd.Parameters.AddWithValue("@Id", medicament.Id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();

        }

        public void deleteMedicament(int id)
        {
            string query = "DELETE FROM medicaments WHERE Id=@Id";
            SqlCommand cmd = new SqlCommand(query,con);
            cmd.Parameters.AddWithValue("@Id", id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public int nombreMedicament()
        {
            string query = "SELECT COUNT(*) as 'nbr'  FROM medicaments";
            SqlCommand cmd = new SqlCommand(query, con);
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                nbrMedicament = (int)reader["nbr"];
            }
            
            reader.Close();
            con.Close();
            return nbrMedicament;
        }

    }
}
