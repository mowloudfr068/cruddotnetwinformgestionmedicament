﻿namespace WindowsFormsGestionMedicamentDB
{
    partial class FormClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewCl1 = new System.Windows.Forms.DataGridView();
            this.textBoxCl1 = new System.Windows.Forms.TextBox();
            this.textBoxCl3 = new System.Windows.Forms.TextBox();
            this.textBoxCl2 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelCl3 = new System.Windows.Forms.Label();
            this.labelCl2 = new System.Windows.Forms.Label();
            this.labelCl1 = new System.Windows.Forms.Label();
            this.buttonCl4 = new System.Windows.Forms.Button();
            this.buttonCl3 = new System.Windows.Forms.Button();
            this.buttonCl2 = new System.Windows.Forms.Button();
            this.buttonCl1 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.homeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCl1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewCl1
            // 
            this.dataGridViewCl1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCl1.Location = new System.Drawing.Point(22, 56);
            this.dataGridViewCl1.Name = "dataGridViewCl1";
            this.dataGridViewCl1.Size = new System.Drawing.Size(413, 249);
            this.dataGridViewCl1.TabIndex = 9;
            // 
            // textBoxCl1
            // 
            this.textBoxCl1.Location = new System.Drawing.Point(87, 30);
            this.textBoxCl1.Name = "textBoxCl1";
            this.textBoxCl1.Size = new System.Drawing.Size(150, 20);
            this.textBoxCl1.TabIndex = 1;
            // 
            // textBoxCl3
            // 
            this.textBoxCl3.Location = new System.Drawing.Point(87, 124);
            this.textBoxCl3.Name = "textBoxCl3";
            this.textBoxCl3.Size = new System.Drawing.Size(150, 20);
            this.textBoxCl3.TabIndex = 2;
            // 
            // textBoxCl2
            // 
            this.textBoxCl2.Location = new System.Drawing.Point(87, 76);
            this.textBoxCl2.Name = "textBoxCl2";
            this.textBoxCl2.Size = new System.Drawing.Size(150, 20);
            this.textBoxCl2.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labelCl3);
            this.groupBox2.Controls.Add(this.labelCl2);
            this.groupBox2.Controls.Add(this.labelCl1);
            this.groupBox2.Controls.Add(this.textBoxCl1);
            this.groupBox2.Controls.Add(this.textBoxCl3);
            this.groupBox2.Controls.Add(this.textBoxCl2);
            this.groupBox2.Location = new System.Drawing.Point(523, 98);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(254, 170);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ajouter client";
            // 
            // labelCl3
            // 
            this.labelCl3.AutoSize = true;
            this.labelCl3.Location = new System.Drawing.Point(22, 127);
            this.labelCl3.Name = "labelCl3";
            this.labelCl3.Size = new System.Drawing.Size(58, 13);
            this.labelCl3.TabIndex = 6;
            this.labelCl3.Text = "Telephone";
            // 
            // labelCl2
            // 
            this.labelCl2.AutoSize = true;
            this.labelCl2.Location = new System.Drawing.Point(22, 79);
            this.labelCl2.Name = "labelCl2";
            this.labelCl2.Size = new System.Drawing.Size(43, 13);
            this.labelCl2.TabIndex = 5;
            this.labelCl2.Text = "Prenom";
            // 
            // labelCl1
            // 
            this.labelCl1.AutoSize = true;
            this.labelCl1.Location = new System.Drawing.Point(22, 30);
            this.labelCl1.Name = "labelCl1";
            this.labelCl1.Size = new System.Drawing.Size(29, 13);
            this.labelCl1.TabIndex = 4;
            this.labelCl1.Text = "Nom";
            // 
            // buttonCl4
            // 
            this.buttonCl4.Location = new System.Drawing.Point(345, 325);
            this.buttonCl4.Name = "buttonCl4";
            this.buttonCl4.Size = new System.Drawing.Size(75, 23);
            this.buttonCl4.TabIndex = 14;
            this.buttonCl4.Text = "Ajouter";
            this.buttonCl4.UseVisualStyleBackColor = true;
            this.buttonCl4.Click += new System.EventHandler(this.buttonCl4_Click);
            // 
            // buttonCl3
            // 
            this.buttonCl3.Location = new System.Drawing.Point(228, 325);
            this.buttonCl3.Name = "buttonCl3";
            this.buttonCl3.Size = new System.Drawing.Size(75, 23);
            this.buttonCl3.TabIndex = 13;
            this.buttonCl3.Text = "Modifier";
            this.buttonCl3.UseVisualStyleBackColor = true;
            this.buttonCl3.Click += new System.EventHandler(this.buttonCl3_Click);
            // 
            // buttonCl2
            // 
            this.buttonCl2.Location = new System.Drawing.Point(134, 325);
            this.buttonCl2.Name = "buttonCl2";
            this.buttonCl2.Size = new System.Drawing.Size(75, 23);
            this.buttonCl2.TabIndex = 12;
            this.buttonCl2.Text = "Supprimer";
            this.buttonCl2.UseVisualStyleBackColor = true;
            this.buttonCl2.Click += new System.EventHandler(this.buttonCl2_Click);
            // 
            // buttonCl1
            // 
            this.buttonCl1.Location = new System.Drawing.Point(453, 158);
            this.buttonCl1.Name = "buttonCl1";
            this.buttonCl1.Size = new System.Drawing.Size(64, 23);
            this.buttonCl1.TabIndex = 11;
            this.buttonCl1.Text = "<";
            this.buttonCl1.UseVisualStyleBackColor = true;
            this.buttonCl1.Click += new System.EventHandler(this.buttonCl1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.LightSeaGreen;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.homeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 29);
            this.menuStrip1.TabIndex = 15;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // homeToolStripMenuItem
            // 
            this.homeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Menu;
            this.homeToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Emoji", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.homeToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Aqua;
            this.homeToolStripMenuItem.Name = "homeToolStripMenuItem";
            this.homeToolStripMenuItem.Size = new System.Drawing.Size(68, 25);
            this.homeToolStripMenuItem.Text = "Home";
            this.homeToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.homeToolStripMenuItem.Click += new System.EventHandler(this.homeToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(677, 44);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // FormClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.dataGridViewCl1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.buttonCl4);
            this.Controls.Add(this.buttonCl3);
            this.Controls.Add(this.buttonCl2);
            this.Controls.Add(this.buttonCl1);
            this.Name = "FormClient";
            this.Text = "FormClient";
            this.Load += new System.EventHandler(this.FormClient_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCl1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewCl1;
        private System.Windows.Forms.TextBox textBoxCl1;
        private System.Windows.Forms.TextBox textBoxCl3;
        private System.Windows.Forms.TextBox textBoxCl2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label labelCl3;
        private System.Windows.Forms.Label labelCl2;
        private System.Windows.Forms.Label labelCl1;
        private System.Windows.Forms.Button buttonCl4;
        private System.Windows.Forms.Button buttonCl3;
        private System.Windows.Forms.Button buttonCl2;
        private System.Windows.Forms.Button buttonCl1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem homeToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}