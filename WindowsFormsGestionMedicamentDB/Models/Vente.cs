﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsGestionMedicamentDB.Models
{
    internal class Vente
    {
        public int Id { get; set; }
        public int Medicament_Id { get; set; }
        public int Client_Id { get; set; }
        public int Quantite { get; set; }

        public DateTime Created_at { get; set; }
    }
}
