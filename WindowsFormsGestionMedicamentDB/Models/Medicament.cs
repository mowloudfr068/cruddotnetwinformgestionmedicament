﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsGestionMedicamentDB.Models
{
    internal class Medicament
    {
        public int Id { get; set; }
        public string Libelle { get; set; }
        public int Prix { get; set; }
        public int Quantite { get; set; }
        

    }
}
