﻿namespace WindowsFormsGestionMedicamentDB
{
    partial class FormVente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewVen1 = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBoxQuantVen = new System.Windows.Forms.TextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.labelVen3 = new System.Windows.Forms.Label();
            this.labelVen2 = new System.Windows.Forms.Label();
            this.labelVen1 = new System.Windows.Forms.Label();
            this.buttonVen4 = new System.Windows.Forms.Button();
            this.buttonVen3 = new System.Windows.Forms.Button();
            this.buttonVen2 = new System.Windows.Forms.Button();
            this.buttonVen1 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.homeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewVen1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewVen1
            // 
            this.dataGridViewVen1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewVen1.Location = new System.Drawing.Point(22, 50);
            this.dataGridViewVen1.Name = "dataGridViewVen1";
            this.dataGridViewVen1.Size = new System.Drawing.Size(413, 249);
            this.dataGridViewVen1.TabIndex = 15;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBoxQuantVen);
            this.groupBox3.Controls.Add(this.comboBox2);
            this.groupBox3.Controls.Add(this.comboBox1);
            this.groupBox3.Controls.Add(this.labelVen3);
            this.groupBox3.Controls.Add(this.labelVen2);
            this.groupBox3.Controls.Add(this.labelVen1);
            this.groupBox3.Location = new System.Drawing.Point(523, 92);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(254, 170);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ajouter ventes";
            // 
            // textBoxQuantVen
            // 
            this.textBoxQuantVen.Location = new System.Drawing.Point(87, 127);
            this.textBoxQuantVen.Name = "textBoxQuantVen";
            this.textBoxQuantVen.Size = new System.Drawing.Size(150, 20);
            this.textBoxQuantVen.TabIndex = 9;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(87, 71);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(150, 21);
            this.comboBox2.TabIndex = 8;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(87, 27);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(150, 21);
            this.comboBox1.TabIndex = 7;
            // 
            // labelVen3
            // 
            this.labelVen3.AutoSize = true;
            this.labelVen3.Location = new System.Drawing.Point(27, 127);
            this.labelVen3.Name = "labelVen3";
            this.labelVen3.Size = new System.Drawing.Size(47, 13);
            this.labelVen3.TabIndex = 6;
            this.labelVen3.Text = "Quantite";
            // 
            // labelVen2
            // 
            this.labelVen2.AutoSize = true;
            this.labelVen2.Location = new System.Drawing.Point(22, 79);
            this.labelVen2.Name = "labelVen2";
            this.labelVen2.Size = new System.Drawing.Size(65, 13);
            this.labelVen2.TabIndex = 5;
            this.labelVen2.Text = "Medicament";
            // 
            // labelVen1
            // 
            this.labelVen1.AutoSize = true;
            this.labelVen1.Location = new System.Drawing.Point(22, 30);
            this.labelVen1.Name = "labelVen1";
            this.labelVen1.Size = new System.Drawing.Size(33, 13);
            this.labelVen1.TabIndex = 4;
            this.labelVen1.Text = "Client";
            // 
            // buttonVen4
            // 
            this.buttonVen4.Location = new System.Drawing.Point(313, 319);
            this.buttonVen4.Name = "buttonVen4";
            this.buttonVen4.Size = new System.Drawing.Size(75, 23);
            this.buttonVen4.TabIndex = 20;
            this.buttonVen4.Text = "Ajouter";
            this.buttonVen4.UseVisualStyleBackColor = true;
            this.buttonVen4.Click += new System.EventHandler(this.buttonVen4_Click);
            // 
            // buttonVen3
            // 
            this.buttonVen3.Location = new System.Drawing.Point(188, 319);
            this.buttonVen3.Name = "buttonVen3";
            this.buttonVen3.Size = new System.Drawing.Size(75, 23);
            this.buttonVen3.TabIndex = 19;
            this.buttonVen3.Text = "Modifier";
            this.buttonVen3.UseVisualStyleBackColor = true;
            this.buttonVen3.Click += new System.EventHandler(this.buttonVen3_Click);
            // 
            // buttonVen2
            // 
            this.buttonVen2.Location = new System.Drawing.Point(72, 319);
            this.buttonVen2.Name = "buttonVen2";
            this.buttonVen2.Size = new System.Drawing.Size(75, 23);
            this.buttonVen2.TabIndex = 18;
            this.buttonVen2.Text = "Supprimer";
            this.buttonVen2.UseVisualStyleBackColor = true;
            this.buttonVen2.Click += new System.EventHandler(this.buttonVen2_Click);
            // 
            // buttonVen1
            // 
            this.buttonVen1.Location = new System.Drawing.Point(453, 152);
            this.buttonVen1.Name = "buttonVen1";
            this.buttonVen1.Size = new System.Drawing.Size(64, 23);
            this.buttonVen1.TabIndex = 17;
            this.buttonVen1.Text = "<";
            this.buttonVen1.UseVisualStyleBackColor = true;
            this.buttonVen1.Click += new System.EventHandler(this.buttonVen1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.LightSeaGreen;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.homeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 29);
            this.menuStrip1.TabIndex = 21;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // homeToolStripMenuItem
            // 
            this.homeToolStripMenuItem.BackColor = System.Drawing.SystemColors.Menu;
            this.homeToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Emoji", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.homeToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Aqua;
            this.homeToolStripMenuItem.Name = "homeToolStripMenuItem";
            this.homeToolStripMenuItem.Size = new System.Drawing.Size(68, 25);
            this.homeToolStripMenuItem.Text = "Home";
            this.homeToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.homeToolStripMenuItem.Click += new System.EventHandler(this.homeToolStripMenuItem_Click);
            // 
            // FormVente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.dataGridViewVen1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.buttonVen4);
            this.Controls.Add(this.buttonVen3);
            this.Controls.Add(this.buttonVen2);
            this.Controls.Add(this.buttonVen1);
            this.Name = "FormVente";
            this.Text = "FormVente";
            this.Load += new System.EventHandler(this.FormVente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewVen1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewVen1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label labelVen3;
        private System.Windows.Forms.Label labelVen2;
        private System.Windows.Forms.Label labelVen1;
        private System.Windows.Forms.Button buttonVen4;
        private System.Windows.Forms.Button buttonVen3;
        private System.Windows.Forms.Button buttonVen2;
        private System.Windows.Forms.Button buttonVen1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBoxQuantVen;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem homeToolStripMenuItem;
    }
}