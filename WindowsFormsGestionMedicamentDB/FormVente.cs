﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsGestionMedicamentDB.Models;
using WindowsFormsGestionMedicamentDB.Repository;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace WindowsFormsGestionMedicamentDB
{
    public partial class FormVente : Form
    {
        bool isModified;
        VenteRepository venteRepository;
        MedicamentRepository medicamentRepository;
        ClientRepository clientRepository;
        public FormVente()
        {
            InitializeComponent();
            venteRepository = new VenteRepository();
            clientRepository = new ClientRepository();
            medicamentRepository = new MedicamentRepository();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormApp formApp = new FormApp();
            formApp.Show();
            FormVente formVente = new FormVente();
            formVente.Close();
        }

        private void FormVente_Load(object sender, EventArgs e)
        {
            LoadVentes();
            comboBox1.Enabled = false;
            comboBox2.Enabled = false;
            textBoxQuantVen.Enabled = false;
            List<Client> clients = new List<Client>();
            clients = clientRepository.AfficherClient();
            for (int i = 0; i < clients.Count; i++)
            {
                comboBox1.Items.Add(clients[i].Id);
            }
            List<Medicament> medicaments = new List<Medicament>();
            medicaments = medicamentRepository.AfficherMedicament();
            for (int i = 0; i < clients.Count; i++)
            {
                comboBox2.Items.Add(medicaments[i].Id);
            }

        }

        private void LoadVentes()
        {
            List<Vente> ventesList = venteRepository.AfficherVentes();
            dataGridViewVen1.DataSource = ventesList;
        }

        private void buttonVen4_Click(object sender, EventArgs e)
        {
            comboBox1.Enabled = true;
            comboBox2.Enabled = true;
            textBoxQuantVen.Enabled = true;
        }

        private void buttonVen1_Click(object sender, EventArgs e)
        {
            if (!isModified)
            {
                //---
                if (comboBox1.Text != "" && comboBox2.Text != "" && textBoxQuantVen.Text != "")
                {
                    int quantite;
                    if (int.TryParse(textBoxQuantVen.Text, out quantite))
                    {
                        Vente vente = new Vente()
                        {
                            Medicament_Id = (int)comboBox2.SelectedItem,
                            Client_Id = (int)comboBox1.SelectedItem,
                            Created_at = DateTime.Now,
                            Quantite = int.Parse(textBoxQuantVen.Text)
                        };
                        venteRepository.CreateVente(vente);
                        dataGridViewVen1.DataSource = null;
                        LoadVentes();
                        MessageBox.Show("lajout de vente est fait avec succee", "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        
                        //---
                    }
                    else
                    {
                        MessageBox.Show("Les quantite doivent etre numerique", "Ereur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Veuillez remplir tous les champs", "Ereur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                if (dataGridViewVen1.SelectedRows.Count > 0)
                {
                    if (comboBox1.Text != "" && comboBox2.Text != "" && textBoxQuantVen.Text != "")
                    {
                        int quantite;
                        if (int.TryParse(textBoxQuantVen.Text, out quantite))
                        {
                            int selectedRowIndex = dataGridViewVen1.SelectedRows[0].Index;
                            int venteId = (int)dataGridViewVen1.Rows[selectedRowIndex].Cells["Id"].Value;
                            Vente vente = new Vente()
                            {
                                Id = venteId,
                                Medicament_Id = (int)comboBox2.SelectedItem,
                                Client_Id = (int)comboBox1.SelectedItem,
                                Created_at = DateTime.Now,
                                Quantite = int.Parse(textBoxQuantVen.Text)
                            };
                            venteRepository.UpdateVente(vente);
                            dataGridViewVen1.DataSource = null;
                            LoadVentes();
                            MessageBox.Show("le vente est modifier avec succee", "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("Les quantite doivent etre numerique", "Ereur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Veuillez remplir tous les champs", "Ereur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void buttonVen3_Click(object sender, EventArgs e)
        {
            if (dataGridViewVen1.Rows.Count > 0)
            {
                isModified = true;
                Vente vente = venteRepository.venteList[dataGridViewVen1.CurrentCell.RowIndex];
                comboBox1.Enabled = true;
                comboBox2.Enabled = true;
                textBoxQuantVen.Enabled = true;
                comboBox1.Text = vente.Client_Id.ToString();
                comboBox2.Text = vente.Medicament_Id.ToString();
                textBoxQuantVen.Text = vente.Quantite.ToString();
            }

        }

        private void buttonVen2_Click(object sender, EventArgs e)
        {
            if (dataGridViewVen1.SelectedRows.Count > 0)
            {
                int selectedRowIndex = dataGridViewVen1.SelectedRows[0].Index;
                int venteId = (int)dataGridViewVen1.Rows[selectedRowIndex].Cells["Id"].Value;
                venteRepository.DeleteVente(venteId);
                MessageBox.Show("la suppression de vente est fait avec succee", "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information);


            }
        }

        private void homeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormApp formApp = new FormApp();
            formApp.Show();
            this.Close();
        }
    }
}
