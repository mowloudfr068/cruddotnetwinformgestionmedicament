﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsGestionMedicamentDB.Models;
using WindowsFormsGestionMedicamentDB.Repository;

namespace WindowsFormsGestionMedicamentDB
{
    public partial class FormMedicament : Form
    {
        bool isModified=false;
        private MedicamentRepository medicamentRepository;
        public FormMedicament()
        {
            InitializeComponent();
            medicamentRepository = new MedicamentRepository();
        }

        private void FormMedicament_Load(object sender, EventArgs e)
        {
            textBox1.Enabled = false;
            textBox2.Enabled = false;
            textBox3.Enabled = false;
            loadMedicaments();
        }

        public void loadMedicaments()
        {
            List<Medicament> medicamentsList = medicamentRepository.AfficherMedicament();
            dataGridView1.DataSource = medicamentsList;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Enabled = true;
            textBox2.Enabled = true;
            textBox3.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!isModified)
            {
                if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "")
                {
                    int prix, quntite;
                    String libelle = textBox1.Text;
                    if (int.TryParse(textBox2.Text, out prix) && int.TryParse(textBox3.Text, out quntite))
                    {
                        Medicament medicament = new Medicament()
                        {
                            Libelle = textBox1.Text,
                            Prix = int.Parse(textBox2.Text),
                            Quantite = int.Parse(textBox3.Text),
                        };
                        medicamentRepository.createMedicament(medicament);
                        dataGridView1.DataSource = null;
                        loadMedicaments();
                        MessageBox.Show("medicament ajouter avec succes", "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    else
                    {
                      MessageBox.Show("Les prix/quantite doivent etre numerique", "Ereur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }


                }
                else
                {
                    MessageBox.Show("Veuillez remplir tous les champs", "Ereur", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }



            }
            else
            {
                if (dataGridView1.SelectedRows.Count > 0)
                {
                    if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "")
                    {
                        int prix, quntite;
                        String libelle = textBox1.Text;
                        if (int.TryParse(textBox2.Text, out prix) && int.TryParse(textBox3.Text, out quntite))
                        {
                            //---
                            int selectedRowIndex = dataGridView1.SelectedRows[0].Index;
                            int medicamentId = (int)dataGridView1.Rows[selectedRowIndex].Cells["Id"].Value;
                            Medicament medicament = new Medicament()
                            {
                                Id = medicamentId,
                                Libelle = textBox1.Text,
                                Prix = Convert.ToInt32(textBox2.Text),
                                Quantite = Convert.ToInt32(textBox3.Text),
                            };
                            medicamentRepository.UpdateMedicament(medicament);
                            dataGridView1.DataSource = null;
                            loadMedicaments();
                            MessageBox.Show("le medicament est modifier avec succee", "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //---
                        }
                        else
                        {
                            MessageBox.Show("Les prix/quantite doivent etre numerique", "Ereur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Veuillez remplir tous les champs", "Ereur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
            }



        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            if (dataGridView1.Rows.Count>0)
            {
                isModified = true;
                Medicament medicament = medicamentRepository.medicamentList[dataGridView1.CurrentCell.RowIndex];
                textBox1.Enabled = true;
                textBox2.Enabled = true;
                textBox3.Enabled= true;
                textBox1.Text = medicament.Libelle;
                textBox2.Text = medicament.Prix.ToString();
                textBox3.Text =  medicament.Quantite.ToString();
            }
           
            

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int selectedRowIndex = dataGridView1.SelectedRows[0].Index;
                int medicamentId = (int) dataGridView1.Rows[selectedRowIndex].Cells["Id"].Value;
                medicamentRepository.deleteMedicament(medicamentId);
                dataGridView1.DataSource = null;
                loadMedicaments();
                MessageBox.Show("la suppression de medicament est fait avec succee","Succes",MessageBoxButtons.OK, MessageBoxIcon.Information);
                

            }
        }

        private void homeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormApp formApp = new FormApp();
            formApp.Show();
            this.Close();
        }
    }
}
